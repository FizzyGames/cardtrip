﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEditor;

namespace CardTrip.CardGame
{
    public class CardGame
    {
        public Player Player1 { get; set; }
        public AIOpponent Opponent { get; set; }

    }

    public class Player
    {
        public virtual IntReactiveProperty PlayerSpeed { get; set; }
        public virtual IntReactiveProperty Wear { get; set; }
        public virtual IntReactiveProperty MaxEnergy { get; set; }
        public virtual IntReactiveProperty Energy { get; set; }
        public virtual ReactiveCollection<Card> Deck { get; set; }
        public virtual ReactiveCollection<Card> Hand { get; set; }
        public virtual ReactiveCollection<Card> Discard { get; set; }
        public virtual ReactiveCollection<Card> Retired { get; set; }
    }
    public class AIOpponent : Player
    {

    }

    public class Turn
    {

    }

    public enum RelativeType
    {
        //greater or equal
        GreaterOrEqual,
        //less or equal
        LowerOrEqual,
        Exact,
        //not valid for cards
        GreaterOnly,
        //not valid for cards
        LowerOnly
    }

    public static class RelativeTypeExtension
    {
        public static bool Evaluate(this RelativeType relative, int a, int b)
        {
            switch (relative)
            {
                case RelativeType.GreaterOrEqual:
                    return a >= b;
                case RelativeType.LowerOrEqual:
                    return a <= b;
                case RelativeType.Exact:
                    return a == b;
                case RelativeType.GreaterOnly:
                    return a > b;
                case RelativeType.LowerOnly:
                    return a < b;
                default:
                    throw new Exception();
            }
        }
    }

    [Serializable]
    public class AppropriateGear
    {
        public RelativeType RelativeType;

        public int GearRelativeTo;
        public AppropriateGear()
        {
            GearRelativeTo = 2;
            RelativeType = RelativeType.GreaterOrEqual;
        }

        public AppropriateGear(RelativeType relativeType, int gearRelativeTo)
        {
            RelativeType = relativeType;
            GearRelativeTo = gearRelativeTo;
        }
    }

    public class CarAvatar
    {

    }

}