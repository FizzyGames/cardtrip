﻿using System.Reflection.Emit;
using UniRx;
using UnityEngine;
using XNode;

namespace CardTrip.CardGame
{
    public class CardNode : Node
    {
        [SerializeField] string _name;
        [SerializeField] [Input] int _correctCost;
        [SerializeField] [Input] int _forcedCost;
        [SerializeField] [Input] RelativeType _relativeType;
        [SerializeField] [Input] int _gearRelativeTo;
        [SerializeField] [Input(ShowBackingValue.Never, typeConstraint = TypeConstraint.Inherited)] bool _conditions = true;
        [SerializeField] [Input(dynamicPortList = true, typeConstraint = TypeConstraint.Inherited)] CardEffect _cardEffects;
        [SerializeField] CardRarity _rarity;
        //[SerializeField] [Input(]

        protected override void Init()
        {
            base.Init();
            var currGraph = base.graph as Card;
            Debug.Assert(currGraph);
            if (!currGraph.InternalCard)
                currGraph.InternalCard = this;
        }

        public string Name => _name;
        public int CorrectCost => GetInputValue<int>(nameof(_correctCost));
        public int ForcedCost => GetInputValue<int>(nameof(_forcedCost));
        public RelativeType RelativeType => GetInputValue<RelativeType>(nameof(_relativeType));
        public int GearRelativeTo => GetInputValue<int>(nameof(_gearRelativeTo));
        public bool AdditionalConditionsMet => GetInputValue<bool>(nameof(_conditions));
        public CardRarity Rarity => _rarity;
    }

    public enum TargetType
    {
        Self,
        Target
    }
}