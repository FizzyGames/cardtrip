﻿using UnityEngine;

namespace CardTrip.CardGame
{
    [System.Serializable]
    public class IntMathNode : XNode.Node
    {
        // Adding [Input] or [Output] is all you need to do to register a field as a valid port on your node 
        [Input] public int a;
        [Input] public int b;
        // The value of an output node field is not used for anything, but could be used for caching output results
        [Output] public int result;

        // Will be displayed as an editable field - just like the normal inspector
        public MathType mathType = MathType.Add;
        public enum MathType { Add, Subtract, Multiply, Divide }

        // GetValue should be overridden to return a value for any specified output port
        public override object GetValue(XNode.NodePort port)
        {

            // Get new a and b values from input connections. Fallback to field values if input is not connected
            int a = GetInputValue<int>("a", this.a);
            int b = GetInputValue<int>("b", this.b);

            // After you've gotten your input values, you can perform your calculations and return a value
            result = 0;
            if (port.fieldName == "result")
                switch (mathType)
                {
                    case MathType.Add: default: result = a + b; break;
                    case MathType.Subtract: result = a - b; break;
                    case MathType.Multiply: result = a * b; break;
                    case MathType.Divide: result = a / b; break;
                }
            return result;
        }
    }
}