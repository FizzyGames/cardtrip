﻿using UniRx;
using UnityEngine;
using XNode;

namespace CardTrip.CardGame
{
    public class CurrentEnergyNode : Node
    {
        [SerializeField] [Output(ShowBackingValue.Never)] int _value;
        public override object GetValue(NodePort port)
        {
            var owner = base.graph as IHasCardGame;
            if (port.fieldName == "_value") return owner.CardGame?.Player1.Energy.Value ?? 1;
            else return null;
        }
    }
}