﻿using UnityEngine;

namespace CardTrip.CardGame
{
    public class ApplyBuffEffect : CardEffect
    {
        [SerializeField] BuffNode _buff;
        [SerializeField] TargetType _targetType;
        [Input] [SerializeField] int _buffStackCount = 2;
    }
}