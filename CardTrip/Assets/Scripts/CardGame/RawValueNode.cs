﻿using UniRx;
using UnityEngine;
using XNode;

namespace CardTrip.CardGame
{
    public class RawValueNode : Node
    {
        [SerializeField] [Output(ShowBackingValue.Always)] int _value;
        public override object GetValue(NodePort port)
        {
            if (port.fieldName == "_value") return _value;
            else return null;
        }
    }
}