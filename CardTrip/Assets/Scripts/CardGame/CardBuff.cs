﻿using UnityEngine;
using XNode;

namespace CardTrip.CardGame
{
    [CreateAssetMenu(fileName = "NewBuff", menuName = "Buff", order = 52)]
    [RequireNode(typeof(BuffNode))]
    public class CardBuff : NodeGraph
    {

    }
}