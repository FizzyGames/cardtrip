﻿using UniRx;
using UnityEngine;
using XNode;

namespace CardTrip.CardGame
{
    [CreateAssetMenu(fileName = "NewCard", menuName = "Card", order = 51)]
    [RequireNode(typeof(CardNode))]
    public class Card : NodeGraph, IHasCardGame
    {

        private CardGame _game;
        public CardGame CardGame => _game;

        public override void Clear()
        {
            base.Clear();
        }

        public Card CreateClone(CardGame cardGame)
        {
            var newClone = Instantiate(this);
            newClone._game = cardGame;
            return newClone;
        }

        public AppropriateGear AppropriateGear => new AppropriateGear(InternalCard.RelativeType, InternalCard.GearRelativeTo);
        public int CorrectCost => InternalCard.CorrectCost;
        public int ForcedCost => InternalCard.ForcedCost;
        public CardNode InternalCard { get; set; }
    }

    public interface IHasCardGame
    {
        CardGame CardGame { get; }
    }

    public enum CardRarity
    {
        Common,
        Uncommon,
        Rare,
        UltraRare,
        Special,
    }
}