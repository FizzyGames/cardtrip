﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using UniRx;
using UnityEngine;
using XNode;
using static XNode.Node;

namespace CardTrip.CardGame
{
    public class DrawCardEffect : CardEffect
    {
        [Input] [SerializeField] int _cardCount;


    }

    public enum CardZones
    {
        Deck,
        Hand,
        Discard,
        Retired
    }

    public class CardPile : ScriptableObject, IEnumerable<Card>
    {
        [SerializeField]
        private List<Card> _cards = new List<Card>();

        public List<Card> Cards => _cards;

        public IEnumerator<Card> GetEnumerator()
        {
            return Cards.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Cards.GetEnumerator();
        }

        public void Shuffle()
        {
            _cards.Shuffle();
        }
    }

    public static class CollectionUtils
    {
        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = Random.Range(0, n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }

    public class SelectCardsFrom : Node
    {
        [SerializeField] private CardZones _zone;
        [Output(ShowBackingValue.Never)] [SerializeField] private List<Card> _cards;
        public override object GetValue(NodePort port)
        {
            if (port.fieldName == "_value") return _cards;
            else return null;
        }
    }

    public class FilterCardsByNumeric : Node
    {
        [SerializeField] private RelativeType _relativeType;
        [SerializeField] private CardNumericType _numericType;
        [Input] [SerializeField] private int _valueToCompare;
        [Input(ShowBackingValue.Never, ConnectionType.Override, TypeConstraint.Inherited)] [SerializeField] private List<Card> _inputCards;
        [Output(ShowBackingValue.Never)] [SerializeField] private List<Card> _cards;
        public enum CardNumericType
        {
            Gear,
            CorrectCost,
            ForcedCost,
            AnyCost,
        }
        

        private bool ConditionalTrue(Card card)
        {
            switch (_numericType)
            {
                case CardNumericType.Gear:
                    return card.AppropriateGear.RelativeType.Evaluate(card.AppropriateGear.GearRelativeTo, _valueToCompare);
                case CardNumericType.CorrectCost:
                    return _relativeType.Evaluate(card.CorrectCost, _valueToCompare);
                case CardNumericType.ForcedCost:
                    return _relativeType.Evaluate(card.ForcedCost, _valueToCompare);
                case CardNumericType.AnyCost:
                    return _relativeType.Evaluate(card.CorrectCost, _valueToCompare) || _relativeType.Evaluate(card.ForcedCost, _valueToCompare);
                default:
                    return true;
            }
        }

        private List<Card> GetAdjustedList()
        {
            return GetInputValue<List<Card>>(nameof(_inputCards))
                .Where(ConditionalTrue)
                .ToList();
        }

        public override object GetValue(NodePort port)
        {
            if (port.fieldName == "_value") return GetAdjustedList();
            else return null;
        }
    }

    public class MoveCardEffect : CardEffect
    {
        [Input(ShowBackingValue.Never, ConnectionType.Override, TypeConstraint.Inherited)] [SerializeField] private List<Card> _inputCards;
        [SerializeField] private CardZones _destination;
    }

    public class UserSelectsCards : CardEffect
    {
        [Input(ShowBackingValue.Never, ConnectionType.Override, TypeConstraint.Inherited)] [SerializeField] private List<Card> _cardsToMove;
        [Output(ShowBackingValue.Never)] [SerializeField] private List<Card> _cards;

    }
}