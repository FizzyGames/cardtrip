﻿using System;
using UnityEngine;
using XNode;

namespace CardTrip.CardGame
{
    [Serializable]
    public abstract class CardEffect : Node
    {
        [Output(ShowBackingValue.Never)] [SerializeField] CardEffect _effect;

        protected CardGame CardGame => ((IHasCardGame)graph).CardGame;

        public override object GetValue(NodePort port)
        {
            if (port.fieldName == "_effect") return this;
            else return null;
        }
    }
}